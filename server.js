var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechucvc/collections/";
var MLabAPIKey = "apiKey=GYH5GtV22PgLJfdPqdF1sQ1Kuk-vo39H";
var requestJson = require ('request-json');

app.listen(port);
console.log("API molona escuchando en el puerto " + port);

app.get("/apitechu/v1",
  function(req, res){
    console.log("GET /apitechu/v1");
    res.send({"msg":"Hola desde apitechu"});
  }
);

app.get("/apitechu/v1/users",
  function(req, res){
    console.log("GET /apitechu/v1/users");
    //res.sendFile('usuarios.json', {root: __dirname});
    var users = require('./usuarios.json');
    res.send(users);
  }
);

app.post ("/apitechu/v1/users",
   function(req, res){
     console.log("POST /apitechu/v1/users");
//     console.log(req.headers);
     console.log("first_name is "  + req.body.first_name);
     console.log("last_name is "  + req.body.last_name);
     console.log("country is "  + req.body.country);

     var newUser = {
       "first_name" : req.body.first_name,
       "last_name" : req.body.last_name,
       "country" : req.body.country
     };
     var users = require('./usuarios.json');
     //users pasa a ser tipo array
     users.push(newUser);
     writeUserDataToFile(users);
     console.log("Usuario guardado con éxito");
     res.send({"msg" : "Usuario guardado con éxito"});
    }
);

app.delete("/apitechu/v1/users/:id",
   function(req, res){
     console.log("DELETE /apitechu/v1/users/:id");
     console.log(req.params);
     console.log(req.params.id);

    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg" : "Usuario borrado"});
   }
)

function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
  function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("Datos escritos en archivo");
    }
  }
)
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res) {
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Body");
    console.log(req.body);

    console.log("Headers");
    console.log(req.headers);
  }
)

app.post ("/apitechu/v1/login",
   function(req, res){
     console.log("POST /apitechu/v1/login");
     console.log("id is "  + req.body.id);
     console.log("email is "  + req.body.email);
     console.log("password is "  + req.body.password);

  var users = require('./usuarios.json');

  for (user of users){
   if (req.body.email== user.email && req.body.password == user.password){
//    if (req.body.email== user.email){

      user.logged = true;
      console.log("Login correcto");
      console.log("idUsuario " + user.id);
      writeUserDataToFile(users);
      res.send({"msg" : "Login correcto idUsuario: " + user.id});
      }else{
    console.log("Login incorrecto");
      }
     }
  res.send({"msg" : "Login incorrecto"});
}

);

app.post ("/apitechu/v1/logout",
   function(req, res){
     console.log("POST /apitechu/v1/logout");
     console.log("id is "  + req.body.id);


  var users = require('./usuarios.json');

  for (user of users){
   if (req.body.id == user.id && user.logged == true){
      delete user.logged;
      console.log("Logout correcto");
      console.log("Logout correcto"+ user.id);
      writeUserDataToFile(users);
      res.send({"msg" : "Logout correcto idUsuario: " + user.id});
      }else{
    console.log("Logout incorrecto");
      }
     }
  res.send({"msg" : "Logout incorrecto"});
}
);

app.get("/apitechu/v2/users",
  function(req, res){
    console.log("GET /apitechu/v2/users");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + MLabAPIKey,
      function(err, resMLAB, body) {
        var response =!err ? body:{
          "msg" : "Error obteniendo usuarios"
        }
        res.send(response);
      }

  )
  }
);


app.get("/apitechu/v2/users/:id",
  function(req, res){
    console.log("GET /apitechu/v2/users/:id");

    var id = req.params.id;
    var query = 'q={"id":' + id + '}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("user?" + query + "&" + MLabAPIKey,
      function(err, resMLAB, body) {
      //  var response =!err ? body:{
      //    "msg" : "Error obteniendo usuarios"
      //  }
      var response = {};

      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body ;
        } else {
          response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
        res.send(response);
        }
      }

  )
  }
);

app.post ("/apitechu/v2/login",
   function(req, res){
     console.log("POST /apitechu/v2/login");
     console.log("id is "  + req.body.id);
     console.log("email is "  + req.body.email);
     console.log("password is "  + req.body.password);

     var email = req.body.email;
     var password = req.body.password;
     var query = 'q={"email":"' + email +'", "password":"' + password +'"}';
     var putBody = '{"$set":{"logged":true}}';
     var httpClient = requestJson.createClient(baseMLabURL);
     console.log("Cliente HTTP creado");


httpClient.get("user?" + query + "&" + MLabAPIKey,
  function(err, resMLAB, body) {

  var response = {};

  if (err) {
    response = {
      "msg" : "Error obteniendo usuario."
    }
    res.status(500);
  } else {
    if (body.length > 0) {

      httpClient.put("user?" + query + "&" + MLabAPIKey, JSON.parse(putBody),
        function(err, respuesta, body) {
          console.log("usuario logado");

        }
      );
      response = { "msg" : "Usuario logado"};
    } else {
      response = {
        "msg" : "Usuario no encontrado"
      };
      res.status(404);
    }
    res.send(response);
    }
  }

);
}
);

app.post ("/apitechu/v2/logout",
   function(req, res){
     console.log("POST /apitechu/v2/logout");
     console.log("id is "  + req.body.id);
     console.log("email is "  + req.body.email);
     console.log("password is "  + req.body.password);

     var email = req.body.email;
     var password = req.body.password;
     var query = 'q={"email":"' + email +'", "password":"' + password +'"}';
     var putBody = '{"$unset":{"logged":""}}';
     var httpClient = requestJson.createClient(baseMLabURL);


     console.log("Cliente HTTP creado");


httpClient.get("user?" + query + "&" + MLabAPIKey,
  function(err, resMLAB, body) {
    var response = {};
    console.log(body);
  //  console.log(body.email);
  //  console.log(resMLAB)

    if (err) {
      console.log("hubo un error");
      response = {
        "msg" : "Error obteniendo usuario."
      }
      res.status(500);
    } else {
      if (body.length > 0 && body[0].logged == true) {

            console.log("el usuario estaba logado");
            response = {"msg" : "Logout correcto id: " + body[0].id};
            httpClient.put("user?" + query + "&" + MLabAPIKey, JSON.parse(putBody),
              function(err, respuesta, body) {
                console.log("Logout correcto");
              }
            );
         response = {"msg" : "Logout correcto de usuario id: " +body[0].id};
         console.log("Id de usuario" + body[0].id);

      } else {
        response = {"msg" : "Usuario no encontrado"};
        res.status(404);
      }
      res.send(response);
      }
    }
  );
  }
);

app.get("/apitechu/v2/accounts",
  function(req, res){
    console.log("GET /apitechu/v2/accounts");
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("accounts?" + MLabAPIKey,
      function(err, resMLAB, body) {
        var response =!err ? body:{
          "msg" : "Error obteniendo cuentas"
        }
        res.send(response);
      }

  )
  }
);

app.get("/apitechu/v2/users/:id/accounts",
  function(req, res){
    console.log("GET /apitechu/v2/users/:id/accounts");

    var id = req.params.id;
    var query = 'q={"USERID":' + id + '}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente HTTP creado");

    httpClient.get("accounts?" + query + "&" + MLabAPIKey,
      function(err, resMLAB, body) {
      //  var response =!err ? body:{
      //    "msg" : "Error obteniendo usuarios"
      //  }
      var response = {};

      if (err) {
        response = {
          "msg" : "Error obteniendo usuario."
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          response = body[0] ;
        } else {
          response = {
            "msg" : "Usuario no encontrado"
          };
          res.status(404);
        }
        res.send(response);
        }
      }

  )
  }
);
